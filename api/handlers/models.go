package handlers

// структуры для json тел запросов и ответов

// структура тела запроса для добавления пользователя в сегмент
type AddUserSegmentRequestBody struct {
	Add    []SegmentWithTTL `json:"add"`
	Delete []string         `json:"delete"`
}

type SegmentWithTTL struct {
	Slug    string `json:"slug"`
	ExpTime string `json:"exp_time"`
}

// структура тела ответа для получения сегментов пользователя
type ShowUserSegmentsResponceBody struct {
	Segments []string `json:"segments"`
}
