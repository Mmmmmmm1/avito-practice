package handlers

import (
	"avito-practice/internal/app"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

type Handler struct {
	app *app.Application
}

func NewHandler(a *app.Application) Handler {
	return Handler{app: a}
}

// Всегда возвращает 200 (для проверки работоспособности сервера)
func (h *Handler) PingHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func (h *Handler) UpdateTTLHandler(w http.ResponseWriter, r *http.Request) {
	var msg string // ответ сервера
	ttls, err := h.app.Database.GetExpiredTTLs()
	if err != nil {
		msg = "Failed to get expired TTLs: " + err.Error()
		log.Print(msg)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode("{msg:\"" + msg + "\"}")
		return
	}
	// удаляем пользователей из просроченных сегментов
	for _, ttl := range ttls {
		err := h.app.Database.DeleteUserFromSegment(ttl.UserId, ttl.Segment)
		if err != nil {
			msg = "Unable to delete user from segment: " + err.Error()
			log.Print(msg)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode("{msg:\"" + msg + "\"}")
			return
		}

		// добавляем историю
		err = h.app.Database.AddHistory(ttl.UserId, ttl.Segment, "Deletion", time.Now().UTC())
		if err != nil {
			msg = "Unable to add history: " + err.Error()
			log.Print(msg)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(msg)
			return
		}
	}
	jsonMsg := struct {
		Msg string `json:"msg"`
	}{"TTLs updated successfully!"}
	fmt.Print(msg)
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(jsonMsg)
}

// Получение истории действий над пользователем и сегментами в период, время возвращется по указанному в теле запроса часовому поясу
func (h *Handler) ShowUserHistoryHandler(w http.ResponseWriter, r *http.Request) {
	var msg string      // ответ сервера
	vars := mux.Vars(r) //получаем переменные запроса
	id := vars["id"]
	// парсируем даты в требуемом формате
	from, err := time.Parse("2006-01-02", vars["from"])
	if err != nil {
		msg = "Unable to parse query time (from): " + err.Error()
		log.Print(msg)
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(msg)
		return
	}
	to, err := time.Parse("2006-01-02", vars["to"])
	if err != nil {
		msg = "Unable to parse query time (to): " + err.Error()
		log.Print(msg)
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(msg)
		return
	}
	// проверяем есть ли пользователь в бд
	exists, err := h.app.Database.UserExists(id)
	// если возникла какая либо ошибка при поиске пользователя (помимо его отсутствия)
	if err != nil {
		msg = "Unable to find user: " + err.Error()
		log.Print(msg)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(msg)
		return
	}
	// если пользователя нет, отправляем 404
	if !exists {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode("404 page not found")
		return
	}

	// проверяем, правильно ли указан промежуток (начало раньше конца)
	if from.Compare(to) >= 0 {
		msg = "Incorrect query: the beginning of the time period is later than the end"
		log.Print(msg)
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(msg)
		return
	}

	// получаем историю пользователя
	hist, err := h.app.Database.GetHistory(id, from, to)
	if err != nil {
		msg = "Unable to get history: " + err.Error()
		log.Print(msg)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(msg)
		return
	}

	// создаем уникальное имя CSV файла
	filename := fmt.Sprint("user_", id, "_history_", from.Format("2006-01-02"), "-",
		to.Format("2006-01-02"), "_reqTime(UTC+0)_", time.Now().UTC().Format("20060102150405"), ".csv")
	// создаем файл в папке для файлов сайта
	f, err := os.Create("../../web/user_reports/" + filename)
	if err != nil {
		msg = "Unable to create history csv file: " + err.Error()
		log.Print(msg)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(msg)
		return
	}
	defer f.Close()

	// создаем нового писателя в CSV
	csvW := csv.NewWriter(f)
	defer csvW.Flush()

	// заносим имена колонок
	header := []string{"User", "Segment", "Action", "Time (UTC+0)"}
	if err := csvW.Write(header); err != nil {
		msg = "Unable to write history csv file: " + err.Error()
		log.Print(msg)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(msg)
		return
	}
	// заносим саму историю
	for _, record := range hist {
		row := []string{record.UserId, record.Segment, record.Action, record.Time.String()}
		if err := csvW.Write(row); err != nil {
			msg = "Unable to write history csv file: " + err.Error()
			log.Print(msg)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(msg)
			return
		}
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	// в ответ записываем ссылку на файл, доступного через файловый сервер
	fmt.Fprint(w, "http://", h.app.Configuration.Server.OuterHost, ":", h.app.Configuration.Server.Port, "/static/", filename)
}

// Получает список сегментов, в которых находится пользователь по его id
func (h *Handler) ShowUserSegmentsHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r) //получаем переменные запроса
	id := vars["id"]
	var msg string // ответ сервера

	// проверяем есть ли пользователь в бд
	exists, err := h.app.Database.UserExists(id)
	// если возникла какая либо ошибка при поиске пользователя (помимо его отсутствия)
	if err != nil {
		msg = "Unable to find user: " + err.Error()
		log.Print(msg)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(msg)
		return
	}
	// если пользователя нет, отправляем 404
	if !exists {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode("404 page not found")
		return
	}

	// получаем сегменты пользователя
	segments, err := h.app.Database.GetUserSegments(id)
	if err != nil {
		msg = "Unable to show user segments: " + err.Error()
		log.Print(msg)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(msg)
		return
	}

	// записываем сегменты в тело ответа
	responceBody := ShowUserSegmentsResponceBody{Segments: segments}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(responceBody)
}

// Добавляет пользователя в бд
func (h *Handler) AddUserHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r) //получаем переменные запроса
	id := vars["id"]
	// заносим пользователя в бд
	err := h.app.Database.AddUser(id)
	var msg string // ответ сервера
	if err != nil {
		msg = "Unable to add new user: " + err.Error()
		log.Print(msg)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(msg)
		return
	}
	msg = "New user added successfully!"
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(msg)
}

// Добавляет сегмент в бд
func (h *Handler) AddSegmentHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r) //получаем переменные запроса
	slug := vars["slug"]
	percent, _ := strconv.Atoi(vars["percent"])
	// заносим сегмент в бд
	err := h.app.Database.AddSegment(slug)
	var msg string // ответ сервера
	if err != nil {
		msg = "Unable to add new segment: " + err.Error()
		log.Print(msg)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(msg)
		return
	}

	// если в сегмент добавляется 0% пользователей, то ничего не делаем
	if percent > 0 {
		// получаем всех пользователей
		users, err := h.app.Database.GetAllUsers()
		if err != nil {
			msg = "Unable to get all users: " + err.Error()
			log.Print(msg)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(msg)
			return
		}
		// если нужно добавить всех, просто перебираем пользователей
		if percent == 100 {
			for _, user := range users {
				err = h.app.Database.AddUserToSegment(user, slug)
				if err != nil {
					msg = "Unable to add user to segment: " + err.Error()
					log.Print(msg)
					w.WriteHeader(http.StatusInternalServerError)
					json.NewEncoder(w).Encode(msg)
					return
				}
				// добавляем историю
				err = h.app.Database.AddHistory(user, slug, "Addition", time.Now().UTC())
				if err != nil {
					msg = "Unable to add history: " + err.Error()
					log.Print(msg)
					w.WriteHeader(http.StatusInternalServerError)
					json.NewEncoder(w).Encode(msg)
					return
				}
			}
		} else {
			// генерируем массив случайных индексов (от 0 до общего кол-ва пользователей) длины равной количеству % пользователей от общего числа
			targetUsersIds := GenerateUniqueRandomNumbers(len(users)*percent/100, len(users))
			// добавляем % случайных пользователей в сегмент
			for _, id := range targetUsersIds {
				user := users[id]
				err = h.app.Database.AddUserToSegment(user, slug)
				if err != nil {
					msg = "Unable to add user to segment: " + err.Error()
					log.Print(msg)
					w.WriteHeader(http.StatusInternalServerError)
					json.NewEncoder(w).Encode(msg)
					return
				}
				// добавляем историю
				err = h.app.Database.AddHistory(user, slug, "Addition", time.Now().UTC())
				if err != nil {
					msg = "Unable to add history: " + err.Error()
					log.Print(msg)
					w.WriteHeader(http.StatusInternalServerError)
					json.NewEncoder(w).Encode(msg)
					return
				}
			}
		}

	}
	msg = "New segment added successfully!"
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(msg)
}

// Добавляет пользователя в сегменты и удаляет пользователя из сегментов, указанных в теле запроса
func (h *Handler) AddSegmentsToUserHandler(w http.ResponseWriter, r *http.Request) {
	headerContentTtype := r.Header.Get("Content-Type")
	vars := mux.Vars(r) //получаем переменные запроса
	id := vars["id"]
	var msg string // ответ сервера

	// проверяем есть ли пользователь в бд
	exists, err := h.app.Database.UserExists(id)
	// если возникла какая либо ошибка при поиске пользователя (помимо его отсутствия)
	if err != nil {
		msg = "Unable to find user: " + err.Error()
		log.Print(msg)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(msg)
		return
	}
	// если пользователя нет, отправляем 404
	if !exists {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode("404 page not found")
		return
	}

	// проверяем соответсвтвие типа содержимого запроса
	if headerContentTtype != "application/json" {
		msg = "Incorrect content type!"
		log.Print(msg)
		w.WriteHeader(http.StatusUnsupportedMediaType)
		json.NewEncoder(w).Encode(msg)
		return
	}
	// декодируем тело запроса
	var reqB AddUserSegmentRequestBody
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err = decoder.Decode(&reqB)
	if err != nil {
		msg = "Failed to decode request body: " + err.Error()
		log.Print(msg)
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(msg)
		return
	}

	// проверяем, присутствуют ли указанные сегменты
	for _, s := range reqB.Delete {
		exists, err = h.app.Database.SegmentExists(s)
		// если возникла какая либо ошибка при поиске сегмента (помимо его отсутствия)
		if err != nil {
			msg = "Unable to find segment: " + err.Error()
			log.Print(msg)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(msg)
			return
		}
		// если сегмента нет, отправляем StatusInternalServerError
		if !exists {
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode("Unable to add user to segment: requested segment " + s + " does not exist!")
			return
		}
	}

	for _, s := range reqB.Add {
		exists, err = h.app.Database.SegmentExists(s.Slug)
		// если возникла какая либо ошибка при поиске сегмента (помимо его отсутствия)
		if err != nil {
			msg = "Unable to find segment: " + err.Error()
			log.Print(msg)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(msg)
			return
		}
		// если сегмента нет, отправляем StatusInternalServerError
		if !exists {
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode("Unable to add user to segment: requested segment " + s.Slug + " does not exist!")
			return
		}
	}
	// проверяем, есть ли одни и те же сегменты на добавление и удаление
	intersection := FindIntersections(reqB.Add, reqB.Delete)
	if len(intersection) != 0 {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Unable to add user to segment: segments for addition and deletion have the same elements!")
		return
	}

	// проверяем, есть ли повторяющиеся сегменты внутри добавления или удаления
	if anyDupes := FindDuplicateTTL(reqB.Add); anyDupes {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Unable to add user to segment: segments for addition have duplicates!")
		return
	}
	if anyDupes := FindDuplicateStr(reqB.Delete); anyDupes {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode("Unable to add user to segment: segments for deletion have duplicates!")
		return
	}

	// проверяем, есть ли уже у пользователя добавляемый сегмент
	for _, s := range reqB.Add {
		exists, err := h.app.Database.UserSegmentExists(id, s.Slug)
		if err != nil {
			msg = "Unable to check for user segment: " + err.Error()
			log.Print(msg)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(msg)
			return
		}
		// если он есть, то добавлять нечего, ошибка
		if exists {
			msg = "Unable to add user to segment: user already has this segment!"
			log.Print(msg)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(msg)
			return
		}
	}
	// проверяем, имеет ли пользователь удаляемый сегмент
	for _, s := range reqB.Delete {
		exists, err := h.app.Database.UserSegmentExists(id, s)
		if err != nil {
			msg = "Unable to check for user segment: " + err.Error()
			log.Print(msg)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(msg)
			return
		}
		// если его нет, то удалять нечего, ошибка
		if !exists {
			msg = "Unable to delete user from segment: user does not have this segment!"
			log.Print(msg)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(msg)
			return
		}
	}
	// если проверка правильности запроса успешна, добавляем пользователя в сегменты
	for _, s := range reqB.Add {
		err = h.app.Database.AddUserToSegment(id, s.Slug)
		if err != nil {
			msg = "Unable to add user to segment: " + err.Error()
			log.Print(msg)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(msg)
			return
		}
		// если есть время удаления
		if s.ExpTime != "" {
			// парсируем строку в заданном формате
			expTime, err := time.Parse("2006-01-02 15", s.ExpTime)
			if err != nil {
				msg = "Unable to parse time: " + err.Error() + " expirarion date has not been set!"
				log.Print(msg)
				w.WriteHeader(http.StatusBadRequest)
				json.NewEncoder(w).Encode(msg)
				return
			}
			// записываем дату удаления пользователя из сегмента
			err = h.app.Database.AddTTL(s.Slug, id, expTime)
			if err != nil {
				msg = "Unable to add expiring user to segment: " + err.Error()
				log.Print(msg)
				w.WriteHeader(http.StatusInternalServerError)
				json.NewEncoder(w).Encode(msg)
				return
			}
		}
		// добавляем историю
		err = h.app.Database.AddHistory(id, s.Slug, "Addition", time.Now().UTC())
		if err != nil {
			msg = "Unable to add history: " + err.Error()
			log.Print(msg)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(msg)
			return
		}
	}

	// удаляем пользователя из сегментов
	for _, s := range reqB.Delete {
		err = h.app.Database.DeleteUserFromSegment(id, s)
		if err != nil {
			msg = "Unable to delete user from segment: " + err.Error()
			log.Print(msg)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(msg)
			return
		}
		// добавляем историю
		err = h.app.Database.AddHistory(id, s, "Deletion", time.Now().UTC())
		if err != nil {
			msg = "Unable to add history: " + err.Error()
			log.Print(msg)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(msg)
			return
		}
	}
	// ответ сервера
	msg = "User added to and deleted from segments successfully!"
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(msg)
}

// Удаляем пользователя из бд по его идентификатору
func (h *Handler) DeleteUserHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r) //получаем переменные запроса
	id := vars["id"]
	var msg string // ответ сервера

	// проверяем есть ли удаляемый пользователь в бд
	exists, err := h.app.Database.UserExists(id)
	// если возникла какая либо ошибка при поиске пользователя (помимо его отсутствия)
	if err != nil {
		msg = "Unable to find user: " + err.Error()
		log.Print(msg)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(msg)
		return
	}
	// если пользователя нет, отправляем 404
	if !exists {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode("404 page not found")
		return
	}
	// удаляем пользователя из бд
	err = h.app.Database.DeleteUser(id)
	if err != nil {
		msg = "Unable to delete user: " + err.Error()
		log.Print(msg)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(msg)
		return
	}
	msg = "User deleted successfully!"
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(msg)
}

// удаляем сегмент по его slug
func (h *Handler) DeleteSegmentHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r) //получаем переменные запроса
	slug := vars["slug"]
	var msg string // ответ сервера

	// проверяем есть ли удаляемый сегмент в бд
	exists, err := h.app.Database.SegmentExists(slug)
	// если возникла какая либо ошибка при поиске сегмента (помимо его отсутствия)
	if err != nil {
		msg = "Unable to find segment: " + err.Error()
		log.Print(msg)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(msg)
		return
	}
	// если сегмента нет, отправляем 404
	if !exists {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode("404 page not found")
		return
	}
	// удаляем сегмент из бд
	err = h.app.Database.DeleteSegment(slug)
	if err != nil {
		msg = "Unable to delete segment: " + err.Error()
		log.Print(msg)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(msg)
		return
	}
	msg = "Segment deleted successfully!"
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(msg)
}

// функция поиска одинаковых элементов в 2х массивах по хэш мапу
//
// на вход подаются 2 массива для поиска
//
// возвращается массив пересекаемых элементов
func FindIntersections(a []SegmentWithTTL, b []string) []string {
	set := make([]string, 0)
	hash := make(map[string]struct{})

	for _, v := range a {
		hash[v.Slug] = struct{}{}
	}

	for _, v := range b {
		if _, ok := hash[v]; ok {
			set = append(set, v)
		}
	}

	return set
}

// Функция поиска наличия хотя бы одного появления дубликатов. Если найден хотя бы один, функция заверает работу
//
// на вход подается целевой массив
//
// возвращается булевая переменная (true - есть дубликаты, false - нет)
func FindDuplicateStr(strSlice []string) bool {
	allKeys := make(map[string]bool)
	for _, item := range strSlice {
		if _, value := allKeys[item]; value {
			return true
		}
		allKeys[item] = true
	}
	return false
}

func FindDuplicateTTL(strSlice []SegmentWithTTL) bool {
	allKeys := make(map[string]bool)
	for _, item := range strSlice {
		if _, value := allKeys[item.Slug]; value {
			return true
		}
		allKeys[item.Slug] = true
	}
	return false
}

func GenerateUniqueRandomNumbers(n, max int) []int {
	rand.Seed(time.Now().UnixNano())
	set := make(map[int]bool)
	var result []int
	for len(set) < n {
		value := rand.Intn(max)
		if !set[value] {
			set[value] = true
			result = append(result, value)
		}
	}
	return result
}
