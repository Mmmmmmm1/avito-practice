package router

import (
	"avito-practice/api/handlers"
	"avito-practice/internal/app"
	"net/http"

	"github.com/gorilla/mux"
)

// инициализирует роутер и эндпоинты
//
// принимает переменную приложения для передачи хэндлеров
func SetupRouter(app *app.Application) *mux.Router {
	r := mux.NewRouter()
	handlers := handlers.NewHandler(app)
	// создаем новый файловый сервер для хранения отчетов по истории пользователя
	fs := http.FileServer(http.Dir("../../web/user_reports/"))
	// создаем эндпоинты, по которым будет доступен файловый сервер
	r.Handle("/static/", http.StripPrefix("/static/", fs))
	r.Handle("/static/{file}", http.StripPrefix("/static/", fs))

	// эндпоинт проверки работы сервера
	r.HandleFunc("/ping", handlers.PingHandler).Methods("GET")

	r.HandleFunc("/segment/add", handlers.AddSegmentHandler).Methods("POST").
		Queries("slug", "{slug:[A-Za-z0-9]+(?:[-_][A-Za-z0-9]+)*}", "percent", "{percent:(?:(?:1)00|[1-9]?[0-9])}")
	r.HandleFunc("/user/add", handlers.AddUserHandler).Methods("POST").Queries("id", "{id:[0-9]+}")
	r.HandleFunc("/segment/{slug:[A-Za-z0-9]+(?:[-_][A-Za-z0-9]+)*}/delete", handlers.DeleteSegmentHandler).Methods("DELETE")
	r.HandleFunc("/user/{id:[0-9]+}/delete", handlers.DeleteUserHandler).Methods("DELETE")
	r.HandleFunc("/user/{id:[0-9]+}/addsegments", handlers.AddSegmentsToUserHandler).Methods("POST")
	r.HandleFunc("/user/{id:[0-9]+}/segments", handlers.ShowUserSegmentsHandler).Methods("GET")
	r.HandleFunc("/user/{id:[0-9]+}/history", handlers.ShowUserHistoryHandler).Methods("GET").
		Queries("from", "{from:(?:[0-9]{4})-(?:[0-9]{2})-(?:[0-9]{2})}", "to", "{to:(?:[0-9]{4})-(?:[0-9]{2})-(?:[0-9]{2})}")

	r.HandleFunc("/checkTTL", handlers.UpdateTTLHandler).Methods("POST")

	return r
}
