module avito-practice

go 1.20

require (
	github.com/go-sql-driver/mysql v1.7.1
	github.com/gorilla/mux v1.8.0
	golang.org/x/crypto v0.12.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

require (
	github.com/huandu/go-assert v1.1.6
	github.com/stretchr/testify v1.8.4
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/text v0.12.0 // indirect
)
