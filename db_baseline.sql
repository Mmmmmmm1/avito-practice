USE segments;

CREATE TABLE segment (
    slug VARCHAR (180) NOT NULL,
    PRIMARY KEY (slug)
);

CREATE TABLE user (
    id INT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE user_segment (
    segment_slug VARCHAR (180) NOT NULL,
    user_id INT NOT NULL,
    FOREIGN KEY (segment_slug) REFERENCES segment(slug) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE,
    CONSTRAINT Composite_Key PRIMARY KEY (segment_slug, user_id)
);

CREATE TABLE history (
    user_id INT NOT NULL,    
    segment_slug VARCHAR (180) NOT NULL,
    action ENUM(
            "Addition",
            "Deletion"
        ) NOT NULL, 
    date DATETIME NOT NULL,    
    FOREIGN KEY (segment_slug) REFERENCES segment(slug) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE
);

CREATE TABLE user_segment_ttl (
    segment_slug VARCHAR (180) NOT NULL,
    user_id INT NOT NULL,    
    exp_time DATETIME NOT NULL,
    CONSTRAINT Composite_Key FOREIGN KEY (segment_slug, user_id) REFERENCES user_segment(segment_slug, user_id) ON DELETE CASCADE
);