FROM ubuntu:latest

# Добавить конфигурацию и само приложение в докер
ADD ./cmd/webhook-sender/webhook-sender /root/webhook-sender
ADD ./cmd/webhook-sender/config.yml /root/config.yml

# Даем право выполнять файл
RUN chmod 744 /root/webhook-sender

#Install Cron
RUN apt-get update
RUN apt-get -y install cron

# Добавляем задачу вызова вебхука
RUN crontab -l | { cat; echo "*/5 * * * * /root/webhook-sender"; } | crontab -

# Запускаем крон в фоновом режиме для постоянных вызовов
CMD cron -f