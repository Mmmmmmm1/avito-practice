package database

import "time"

// интерфейс базы данных, где прописаны методы взаимодействия с ней
type IDatabase interface {
	// проверка работы бд
	Ping() error
	// добавление сегмента по slug
	AddSegment(slug string) error
	// удаление сегмента по slug
	DeleteSegment(slug string) error
	// проверка наличия сегмента в бд
	SegmentExists(slug string) (bool, error)
	// добавление пользователя по идентификатору
	AddUser(id string) error
	// удаление пользователя по идентификатору
	DeleteUser(id string) error
	// проверка наличия пользователя в бд
	UserExists(id string) (bool, error)
	// добавление пользователя userId в сегмент segment
	AddUserToSegment(userId, segment string) error
	// удаление пользователя userId из сегмента segment
	DeleteUserFromSegment(userId, segment string) error
	// получение сегментов пользователя по идентификатору
	GetUserSegments(id string) ([]string, error)
	// добавление истории добавления/ удаления пользователя из сегмента
	AddHistory(userId, segment, action string, time time.Time) error
	// получение истории добавления/ удаления пользователя из сегмента за период времени
	GetHistory(userId string, from, to time.Time) ([]History, error)
	// получение всех пользователей
	GetAllUsers() ([]string, error)
	// получение информации, каких пользователей пора удалить из каких сегментов
	GetExpiredTTLs() ([]UserTTL, error)
	//
	AddTTL(segment, userId string, expTime time.Time) error
	// проверка наличия сегмента у пользователя в бд
	UserSegmentExists(id, seg string) (bool, error)
}
