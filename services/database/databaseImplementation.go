package database

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

// реализация интерфейса баз данных, содержит ссылку на подключенную бд и имеет методы реализуемого интерфеса
type Database struct {
	db *sql.DB
}

// конструктор новой бд
func NewDatabase(db *sql.DB) *Database {
	return &Database{
		db: db,
	}
}

// проверка работы бд
func (db *Database) Ping() error {
	return db.db.Ping()
}

// получение сегментов пользователя по идентификатору
func (db *Database) GetUserSegments(id string) ([]string, error) {
	segments := []string{}
	var segment string
	sql := fmt.Sprint("SELECT segment_slug FROM user_segment WHERE user_id = ", id, ";")
	res, err := db.db.Query(sql)
	if err != nil {
		return []string{}, err
	}

	// перебираем полученные сегменты
	for res.Next() {
		// читаем сегмент
		if err = res.Scan(&segment); err != nil {
			return []string{}, err
		}
		// добавляем в массив
		segments = append(segments, segment)
	}
	return segments, nil
}

// добавление пользователя по идентификатору
func (db *Database) AddUser(id string) error {
	sql := "INSERT INTO user (id) VALUES (?);"
	_, err := db.db.Exec(sql, id)
	if err != nil {
		return err
	}
	return nil
}

// добавление сегмента по slug
func (db *Database) AddSegment(slug string) error {
	sql := "INSERT INTO segment (slug) VALUES (?);"
	_, err := db.db.Exec(sql, slug)
	if err != nil {
		return err
	}
	return nil
}

// удаление сегмента по slug
func (db *Database) DeleteSegment(slug string) error {
	sql := "DELETE FROM segment WHERE slug = ?;"
	_, err := db.db.Exec(sql, slug)
	if err != nil {
		return err
	}
	return nil
}

// удаление пользователя по идентификатору
func (db *Database) DeleteUser(id string) error {
	sql := "DELETE FROM user WHERE id = ?;"
	_, err := db.db.Exec(sql, id)
	if err != nil {
		return err
	}
	return nil
}

// проверка наличия сегмента в бд
func (db *Database) SegmentExists(slug string) (bool, error) {
	sql := "SELECT * FROM segment WHERE slug = ?;"
	res := db.db.QueryRow(sql, slug)
	var tmp string
	err := res.Scan(&tmp)
	if err != nil {
		if err.Error() == "sql: no rows in result set" {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

// проверка наличия пользователя в бд
func (db *Database) UserExists(id string) (bool, error) {
	sql := "SELECT * FROM user WHERE id = ?;"
	res := db.db.QueryRow(sql, id)
	var tmp string
	err := res.Scan(&tmp)
	if err != nil {
		if err.Error() == "sql: no rows in result set" {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

// проверка наличия сегмента у пользователя в бд
func (db *Database) UserSegmentExists(id, seg string) (bool, error) {
	sql := "SELECT user_id FROM user_segment WHERE user_id = ? and segment_slug = ?;"
	res := db.db.QueryRow(sql, id, seg)
	var tmp string
	err := res.Scan(&tmp)
	if err != nil {
		if err.Error() == "sql: no rows in result set" {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

// получение всех пользователей
func (db *Database) GetAllUsers() ([]string, error) {
	sql := "SELECT id FROM user;"
	res, err := db.db.Query(sql)
	if err != nil {
		return []string{}, err
	}
	// читаем полученных пользователей
	var user string
	users := []string{}
	for res.Next() {
		if err := res.Scan(&user); err != nil {
			return []string{}, err
		}
		users = append(users, user)
	}
	return users, nil
}

// добавление пользователя userId в сегмент segment
func (db *Database) AddUserToSegment(userId, segment string) error {
	sql := "INSERT INTO user_segment (segment_slug, user_id) VALUES (?,?);"
	_, err := db.db.Exec(sql, segment, userId)
	if err != nil {
		return err
	}
	return nil
}

// удаление пользователя user_id из сегмента segment
func (db *Database) DeleteUserFromSegment(userId, segment string) error {
	sql := "DELETE FROM user_segment WHERE segment_slug=? and user_id=?;"
	res, err := db.db.Exec(sql, segment, userId)
	if err != nil {
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if count == 0 {
		return errors.New("user is not in the segment " + segment)
	}
	return nil
}

// добавление истории добавления/ удаления пользователя из сегмента
//
// на вход подается id пользователя, slug сегмента, действие и время действия (UTC+0)
func (db *Database) AddHistory(userId, segment, action string, time time.Time) error {
	sql := "INSERT INTO history (user_id, segment_slug, action, date) VALUES (?,?,?,?)"
	_, err := db.db.Exec(sql, userId, segment, action, time.UTC())
	if err != nil {
		return err
	}
	return nil
}

// получение истории добавления/ удаления пользователя из сегмента за период времени (UTC+0)
//
// на вход подается id пользователя, временной промежуток UTC+0)
func (db *Database) GetHistory(userId string, from, to time.Time) ([]History, error) {
	history := []History{}

	sql := fmt.Sprint("SELECT segment_slug, action, date FROM history WHERE user_id=", userId,
		" and date >= '", from.Format("2006-01-02 15:04:05"), "' and date <= '", to.Format("2006-01-02 15:04:05"), "'")
	res, err := db.db.Query(sql)
	if err != nil {
		return []History{}, err
	}

	// перебираем полученные записи
	for res.Next() {
		row := History{UserId: userId}
		// читаем сегмент
		if err = res.Scan(&row.Segment, &row.Action, &row.Time); err != nil {
			return []History{}, err
		}
		// добавляем в массив
		history = append(history, row)
	}
	return history, nil
}

// получение информации, каких пользователей пора удалить из каких сегментов
func (db *Database) GetExpiredTTLs() ([]UserTTL, error) {
	sql := "SELECT segment_slug, user_id FROM user_segment_ttl WHERE exp_time <= UTC_TIMESTAMP()"
	res, err := db.db.Query(sql)
	if err != nil {
		return []UserTTL{}, err
	}
	ttls := []UserTTL{}
	for res.Next() {
		ttl := UserTTL{}
		if err := res.Scan(&ttl.Segment, &ttl.UserId); err != nil {
			return []UserTTL{}, err
		}
		ttls = append(ttls, ttl)
	}
	return ttls, nil
}

func (db *Database) AddTTL(segment, userId string, expTime time.Time) error {
	sql := "INSERT INTO user_segment_ttl (segment_slug, user_id, exp_time) VALUES (?, ?, ?)"
	_, err := db.db.Exec(sql, segment, userId, expTime)
	if err != nil {
		return err
	}
	return nil
}

// Создает соединение с БД по строке подключения. Проверяет успешность и выводит ошибки, если такие есть.
//
// Возвращает подключенную БД при успешном подкючении, иначе nil
func CreateDatabaseConnectionMySQL(DatabaseConnectionString string) (*sql.DB, error) {
	db, err := sql.Open("mysql", DatabaseConnectionString)
	if err == nil {
		// проверяем, подключились ли к бд
		err := db.Ping()
		if err != nil {
			return nil, err
		}
		return db, nil
	} else {
		return nil, err
	}
}
