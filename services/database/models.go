package database

import "time"

type History struct {
	UserId  string    `json:"user_id"`
	Segment string    `json:"segment"`
	Action  string    `json:"action"`
	Time    time.Time `json:"time"`
}

type UserTTL struct {
	UserId  string
	Segment string
}
