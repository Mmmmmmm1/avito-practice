package main

import (
	"avito-practice/services/configuration"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"
)

func main() {
	// открываем файл логов вызовов
	log, err := os.OpenFile("log.txt", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0640)
	if err != nil {
		fmt.Print(err)
		return
	}
	defer log.Close()

	// читаем конфигурацию
	config, err := configuration.NewConfig("config.yml")
	if err != nil {
		log.WriteString(time.Now().UTC().Format(time.DateTime) + " " + err.Error())
		return
	}
	var (
		receiverPort = ":" + config.Server.Port       // порт сервера
		server       = "http://" + config.Server.Host // адрес сервера
		webhookURL   = server + receiverPort + "/checkTTL"
	)

	// отправляем запрос к вебхуку
	request, err := http.NewRequest("POST", webhookURL, nil)
	if err != nil {
		log.WriteString(time.Now().UTC().Format(time.DateTime) + " " + err.Error())
		return
	}

	resp, err := http.DefaultClient.Do(request)
	if err != nil {
		log.WriteString(time.Now().UTC().Format(time.DateTime) + " " + err.Error())
		return
	}
	defer resp.Body.Close()

	// читаем ответ
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.WriteString(time.Now().UTC().Format(time.DateTime) + " " + err.Error())
		fmt.Print(err)
		return
	}

	// записываем ответ в логи
	msg := time.Now().UTC().Format(time.DateTime) + " " + string(body)
	_, err = log.WriteString(msg)

	if err != nil {
		fmt.Print(err)
		return
	}

}
