package testing

import (
	"avito-practice/api/handlers"
	"avito-practice/services/configuration"
	"bytes"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"testing"
	"time"

	"github.com/huandu/go-assert"
)

type Header struct {
	Name string
	Val  string
}

var (
	conf, _ = configuration.NewConfig("../cmd/segments-app/config.yml")
	server  = "http://" + conf.Server.Host + ":" + conf.Server.Port
)

func TestPing(t *testing.T) {
	result := makeRequest("GET", server+"/ping", nil, []Header{{Name: "Content-Type", Val: "application/json"}})
	defer result.Body.Close()

	if http.StatusOK != result.StatusCode {
		t.Errorf("handler returned wrong status code: got %v want %v",
			result.StatusCode, http.StatusOK)
	}
}

type addSegmentTestTable struct {
	input addSegmentTestInputTable
	want  int
}
type addSegmentTestInputTable struct {
	slug    string
	percent string
}

func TestAddSegmentHandler(t *testing.T) {
	rnd := rand.New(rand.NewSource(time.Now().UnixNano()))
	samename := "test7" + fmt.Sprint(rnd.Int()%1000000000)
	tests := []addSegmentTestTable{
		{addSegmentTestInputTable{slug: "slug" + fmt.Sprint(rnd.Int()%1000000000), percent: "0"}, 200},
		{addSegmentTestInputTable{slug: "slug@$%^@" + fmt.Sprint(rnd.Int()%1000000000), percent: "10"}, 404},
		{addSegmentTestInputTable{slug: "slug" + fmt.Sprint(rnd.Int()%1000000000) + "-", percent: "10"}, 404},
		{addSegmentTestInputTable{slug: "-slug" + fmt.Sprint(rnd.Int()%1000000000), percent: "10"}, 404},
		{addSegmentTestInputTable{slug: "-slug" + fmt.Sprint(rnd.Int()%1000000000) + "-", percent: "10"}, 404},
		{addSegmentTestInputTable{slug: "slug" + fmt.Sprint(rnd.Int()%1000000000) + "_", percent: "10"}, 404},
		{addSegmentTestInputTable{slug: "_slug" + fmt.Sprint(rnd.Int()%1000000000), percent: "10"}, 404},
		{addSegmentTestInputTable{slug: "_slug" + fmt.Sprint(rnd.Int()%1000000000) + "_", percent: "10"}, 404},
		{addSegmentTestInputTable{slug: "TESt_slug" + fmt.Sprint(rnd.Int()%1000000000), percent: "10"}, 200},
		{addSegmentTestInputTable{slug: "TESt-sLug1" + fmt.Sprint(rnd.Int()%1000000000), percent: "10"}, 200},
		{addSegmentTestInputTable{slug: "test1" + fmt.Sprint(rnd.Int()%1000000000), percent: "10"}, 200},
		{addSegmentTestInputTable{slug: "test2" + fmt.Sprint(rnd.Int()%1000000000), percent: "102"}, 404},
		{addSegmentTestInputTable{slug: "test3" + fmt.Sprint(rnd.Int()%1000000000), percent: "-2"}, 404},
		{addSegmentTestInputTable{slug: "test4" + fmt.Sprint(rnd.Int()%1000000000), percent: "02"}, 404},
		{addSegmentTestInputTable{slug: "test5" + fmt.Sprint(rnd.Int()%1000000000), percent: "asd"}, 404},
		{addSegmentTestInputTable{slug: "test6" + fmt.Sprint(rnd.Int()%1000000000), percent: "0"}, 200},
		{addSegmentTestInputTable{slug: samename, percent: "100"}, 200},
		{addSegmentTestInputTable{slug: samename, percent: "100"}, 500},
	}
	for _, test := range tests {
		url := server + "/segment/add?slug=" + test.input.slug + "&percent=" + test.input.percent
		result := makeRequest("POST", url, nil, []Header{{Name: "Content-Type", Val: "application/json"}})
		defer result.Body.Close()

		if test.want != result.StatusCode {
			t.Errorf("handler returned wrong status code: got %v want %v (req: %s)",
				result.StatusCode, test.want, test.input)
		}
	}
}

type addDeleteViewTestTable struct {
	input string
	want  int
}

func TestAddUserHandler(t *testing.T) {
	rnd := rand.New(rand.NewSource(time.Now().UnixNano()))
	samename := "2" + fmt.Sprint(rnd.Int()%10000000)
	tests := []addDeleteViewTestTable{
		{input: "1" + fmt.Sprint(rnd.Int()%10000000), want: 200},
		{input: samename, want: 200},
		{input: "sdsd", want: 404},
		{input: samename, want: 500},
	}
	for _, test := range tests {
		url := server + "/user/add?id=" + test.input
		result := makeRequest("POST", url, nil, []Header{{Name: "Content-Type", Val: "application/json"}})
		defer result.Body.Close()

		if test.want != result.StatusCode {
			t.Errorf("handler returned wrong status code: got %v want %v (req: %s)",
				result.StatusCode, test.want, test.input)
		}
	}
}

func TestDeleteSegmentHandler(t *testing.T) {
	rnd := rand.New(rand.NewSource(time.Now().UnixNano()))
	t1 := "1" + fmt.Sprint(rnd.Int()%1000000000)
	t2 := "2" + fmt.Sprint(rnd.Int()%1000000000)
	segments := []addSegmentTestTable{
		{addSegmentTestInputTable{slug: t1, percent: "0"}, 200},
		{addSegmentTestInputTable{slug: t2, percent: "10"}, 200},
	}
	for _, segment := range segments {
		url := server + "/segment/add?slug=" + segment.input.slug + "&percent=" + segment.input.percent
		result := makeRequest("POST", url, nil, []Header{{Name: "Content-Type", Val: "application/json"}})
		defer result.Body.Close()

		if segment.want != result.StatusCode {
			t.Errorf("handler returned wrong status code: got %v want %v (req: %s)",
				result.StatusCode, segment.want, segment.input)
		}
	}
	tests := []addDeleteViewTestTable{
		{input: t1, want: 200},
		{input: t2, want: 200},
		{input: "3" + fmt.Sprint(rnd.Int()%1000000000), want: 404},
		{input: t2, want: 404},
	}
	for _, test := range tests {
		url := server + "/segment/" + test.input + "/delete"
		result := makeRequest("DELETE", url, nil, []Header{{Name: "Content-Type", Val: "application/json"}})
		defer result.Body.Close()

		if test.want != result.StatusCode {
			t.Errorf("handler returned wrong status code: got %v want %v (req: %s)",
				result.StatusCode, test.want, test.input)
		}
	}
}

func TestDeleteUserHandler(t *testing.T) {
	rnd := rand.New(rand.NewSource(time.Now().UnixNano()))
	t1 := "1" + fmt.Sprint(rnd.Int()%10000000)
	t2 := "2" + fmt.Sprint(rnd.Int()%10000000)
	users := []addDeleteViewTestTable{
		{input: t1},
		{input: t2},
	}
	for _, user := range users {
		url := server + "/user/add?id=" + user.input
		result := makeRequest("POST", url, nil, []Header{{Name: "Content-Type", Val: "application/json"}})
		defer result.Body.Close()

		if http.StatusOK != result.StatusCode {
			t.Errorf("handler returned wrong status code: got %v want %v (req: %s)",
				result.StatusCode, http.StatusOK, user.input)
		}
	}
	tests := []addDeleteViewTestTable{
		{input: t1, want: 200},
		{input: t2, want: 200},
		{input: "3" + fmt.Sprint(rnd.Int()%10000000), want: 404},
		{input: t2, want: 404},
	}
	for _, test := range tests {
		url := server + "/user/" + test.input + "/delete"
		result := makeRequest("DELETE", url, nil, []Header{{Name: "Content-Type", Val: "application/json"}})
		defer result.Body.Close()

		if test.want != result.StatusCode {
			t.Errorf("handler returned wrong status code: got %v want %v (req: %s)",
				result.StatusCode, test.want, test.input)
		}
	}
}

type addSegmentsToUserTest struct {
	user   string
	header Header
	input  handlers.AddUserSegmentRequestBody
	want   int
}

func TestAddSegmentsToUserHandler(t *testing.T) {

	//добавляем пользователей
	rnd := rand.New(rand.NewSource(time.Now().UnixNano()))
	t1 := "1" + fmt.Sprint(rnd.Int()%10000000)
	t2 := "2" + fmt.Sprint(rnd.Int()%10000000)
	users := []addDeleteViewTestTable{
		{input: t1},
	}
	for _, user := range users {
		url := server + "/user/add?id=" + user.input
		result := makeRequest("POST", url, nil, []Header{{Name: "Content-Type", Val: "application/json"}})
		defer result.Body.Close()

		if http.StatusOK != result.StatusCode {
			t.Errorf("handler returned wrong status code: got %v want %v (req: %s)",
				result.StatusCode, http.StatusOK, user.input)
		}
	}

	// добавляем сегменты
	ts1 := "1" + fmt.Sprint(rnd.Int()%1000000000)
	ts2 := "2" + fmt.Sprint(rnd.Int()%1000000000)
	ts3 := "3" + fmt.Sprint(rnd.Int()%1000000000)
	ts4 := "4" + fmt.Sprint(rnd.Int()%1000000000)
	ts5 := "5" + fmt.Sprint(rnd.Int()%1000000000)
	segments := []addSegmentTestTable{
		{addSegmentTestInputTable{slug: ts1, percent: "0"}, 200},
		{addSegmentTestInputTable{slug: ts2, percent: "0"}, 200},
		{addSegmentTestInputTable{slug: ts3, percent: "0"}, 200},
	}
	for _, segment := range segments {
		url := server + "/segment/add?slug=" + segment.input.slug + "&percent=" + segment.input.percent
		result := makeRequest("POST", url, nil, []Header{{Name: "Content-Type", Val: "application/json"}})
		defer result.Body.Close()

		if segment.want != result.StatusCode {
			t.Errorf("handler returned wrong status code: got %v want %v (req: %s)",
				result.StatusCode, segment.want, segment.input)
		}
	}

	tests := []addSegmentsToUserTest{
		{ // user: ts1 ts2
			user:   t1,
			header: Header{Name: "Content-Type", Val: "application/json"},
			input: handlers.AddUserSegmentRequestBody{
				Add: []handlers.SegmentWithTTL{
					{Slug: ts1},
					{Slug: ts2},
				},
				Delete: []string{},
			}, want: 200,
		},
		{ // no segment to add
			user:   t1,
			header: Header{Name: "Content-Type", Val: "application/json"},
			input: handlers.AddUserSegmentRequestBody{
				Add:    []handlers.SegmentWithTTL{{Slug: ts2}},
				Delete: []string{},
			}, want: 500,
		},
		{ // user:
			user:   t1,
			header: Header{Name: "Content-Type", Val: "application/json"},
			input: handlers.AddUserSegmentRequestBody{
				Add:    []handlers.SegmentWithTTL{},
				Delete: []string{ts1, ts2},
			}, want: 200,
		},
		{ // no segment delete
			user:   t1,
			header: Header{Name: "Content-Type", Val: "application/json"},
			input: handlers.AddUserSegmentRequestBody{
				Add:    []handlers.SegmentWithTTL{},
				Delete: []string{ts4},
			}, want: 500,
		},
		{ // no segment add
			user:   t1,
			header: Header{Name: "Content-Type", Val: "application/json"},
			input: handlers.AddUserSegmentRequestBody{
				Add:    []handlers.SegmentWithTTL{{Slug: ts4}},
				Delete: []string{},
			}, want: 500,
		},
		{ // no segment both
			user:   t1,
			header: Header{Name: "Content-Type", Val: "application/json"},
			input: handlers.AddUserSegmentRequestBody{
				Add:    []handlers.SegmentWithTTL{{Slug: ts4}},
				Delete: []string{ts5},
			}, want: 500,
		},
		{ // dupes add
			user:   t1,
			header: Header{Name: "Content-Type", Val: "application/json"},
			input: handlers.AddUserSegmentRequestBody{
				Add:    []handlers.SegmentWithTTL{{Slug: ts1}, {Slug: ts1}},
				Delete: []string{},
			}, want: 400,
		},
		{ // dupes delete
			user:   t1,
			header: Header{Name: "Content-Type", Val: "application/json"},
			input: handlers.AddUserSegmentRequestBody{
				Add:    []handlers.SegmentWithTTL{},
				Delete: []string{ts1, ts1},
			}, want: 400,
		},
		{ // dupes both
			user:   t1,
			header: Header{Name: "Content-Type", Val: "application/json"},
			input: handlers.AddUserSegmentRequestBody{
				Add:    []handlers.SegmentWithTTL{{Slug: ts3}, {Slug: ts3}},
				Delete: []string{ts1, ts1},
			}, want: 400,
		},
		{ // same el
			user:   t1,
			header: Header{Name: "Content-Type", Val: "application/json"},
			input: handlers.AddUserSegmentRequestBody{
				Add:    []handlers.SegmentWithTTL{{Slug: ts3}, {Slug: ts1}},
				Delete: []string{ts3},
			}, want: 400,
		},
		{ // no segment to delete
			user:   t1,
			header: Header{Name: "Content-Type", Val: "application/json"},
			input: handlers.AddUserSegmentRequestBody{
				Add:    []handlers.SegmentWithTTL{{Slug: ts2}, {Slug: ts3}},
				Delete: []string{ts1},
			}, want: 500,
		},
		{ // no user
			user:   t2,
			header: Header{Name: "Content-Type", Val: "application/json"},
			input: handlers.AddUserSegmentRequestBody{
				Add:    []handlers.SegmentWithTTL{},
				Delete: []string{},
			}, want: 404,
		},
		{ // with time
			user:   t1,
			header: Header{Name: "Content-Type", Val: "application/json"},
			input: handlers.AddUserSegmentRequestBody{
				Add:    []handlers.SegmentWithTTL{{Slug: ts2, ExpTime: "2024-01-01 10"}},
				Delete: []string{},
			}, want: 200,
		},
		{ // wrong time
			user:   t1,
			header: Header{Name: "Content-Type", Val: "application/json"},
			input: handlers.AddUserSegmentRequestBody{
				Add:    []handlers.SegmentWithTTL{{Slug: ts1, ExpTime: "2024/01/01 10"}},
				Delete: []string{},
			}, want: 400,
		},
		{ // wrong content
			user:   t1,
			header: Header{Name: "Content-Type", Val: "text/plain"},
			input: handlers.AddUserSegmentRequestBody{
				Add:    []handlers.SegmentWithTTL{{Slug: ts1, ExpTime: "2024-01-01 10"}},
				Delete: []string{},
			}, want: 415,
		},
	}

	for _, test := range tests {
		url := server + "/user/" + test.user + "/addsegments"
		result := makeRequest("POST", url, test.input, []Header{test.header})
		defer result.Body.Close()

		if test.want != result.StatusCode {
			t.Errorf("handler returned wrong status code: got %v want %v (req: %s)",
				result.StatusCode, test.want, test.input)
		}
	}
}

type showSegTestTable struct {
	respB handlers.ShowUserSegmentsResponceBody
	input string
	want  int
}

func TestShowUserSegmentsHandler(t *testing.T) {
	//добавляем пользователей
	rnd := rand.New(rand.NewSource(time.Now().UnixNano()))
	t1 := "81" + fmt.Sprint(rnd.Int()%100000)
	t2 := "82" + fmt.Sprint(rnd.Int()%100000)
	t3 := "83" + fmt.Sprint(rnd.Int()%100000)
	users := []addDeleteViewTestTable{
		{input: t1},
		{input: t2},
	}
	for _, user := range users {
		url := server + "/user/add?id=" + user.input
		result := makeRequest("POST", url, nil, []Header{{Name: "Content-Type", Val: "application/json"}})
		defer result.Body.Close()

		if http.StatusOK != result.StatusCode {
			t.Errorf("handler returned wrong status code: got %v want %v (req: %s)",
				result.StatusCode, http.StatusOK, user.input)
		}
	}

	// добавляем сегменты
	ts1 := "1" + fmt.Sprint(rnd.Int()%1000000000)
	ts2 := "2" + fmt.Sprint(rnd.Int()%1000000000)
	ts3 := "3" + fmt.Sprint(rnd.Int()%1000000000)
	segments := []addSegmentTestTable{
		{addSegmentTestInputTable{slug: ts1, percent: "0"}, 200},
		{addSegmentTestInputTable{slug: ts2, percent: "0"}, 200},
		{addSegmentTestInputTable{slug: ts3, percent: "0"}, 200},
	}
	for _, segment := range segments {
		url := server + "/segment/add?slug=" + segment.input.slug + "&percent=" + segment.input.percent
		result := makeRequest("POST", url, nil, []Header{{Name: "Content-Type", Val: "application/json"}})
		defer result.Body.Close()

		if segment.want != result.StatusCode {
			t.Errorf("handler returned wrong status code: got %v want %v (req: %s)",
				result.StatusCode, segment.want, segment.input)
		}
	}

	// добавляем пользователей к сегментам
	userseg := []addSegmentsToUserTest{
		{ // user: ts1 ts2
			user:   t1,
			header: Header{Name: "Content-Type", Val: "application/json"},
			input: handlers.AddUserSegmentRequestBody{
				Add: []handlers.SegmentWithTTL{
					{Slug: ts1},
					{Slug: ts2},
				},
				Delete: []string{},
			}, want: 200,
		},
	}

	for _, entry := range userseg {
		url := server + "/user/" + entry.user + "/addsegments"
		result := makeRequest("POST", url, entry.input, []Header{entry.header})
		defer result.Body.Close()

		if entry.want != result.StatusCode {
			t.Errorf("handler returned wrong status code: got %v want %v (req: %s)",
				result.StatusCode, entry.want, entry.input)
		}
	}

	tests := []showSegTestTable{
		{
			respB: handlers.ShowUserSegmentsResponceBody{Segments: []string{ts1, ts2}},
			input: t1,
			want:  200,
		},
		{
			respB: handlers.ShowUserSegmentsResponceBody{Segments: []string{}},
			input: t2,
			want:  200,
		},
		{
			respB: handlers.ShowUserSegmentsResponceBody{Segments: []string{}},
			input: t3,
			want:  404,
		},
	}
	for _, test := range tests {
		url := server + "/user/" + test.input + "/segments"
		result := makeRequest("GET", url, nil, []Header{})
		defer result.Body.Close()

		if test.want != result.StatusCode {
			t.Errorf("handler returned wrong status code: got %v want %v (req: %s)",
				result.StatusCode, test.want, test.input)
		}

		// если успешно вернулся список
		if result.StatusCode == 200 {
			body, err := io.ReadAll(result.Body)
			if err != nil {
				t.Errorf("failed to parse responce: %s  (req: %s)", err, test.input)
			}
			var responseBody handlers.ShowUserSegmentsResponceBody
			err = json.Unmarshal(body, &responseBody)
			if err != nil {
				t.Errorf("failed to parse responce: %s  (req: %s)", err, test.input)
			}
			assert.Equal(t, test.respB.Segments, responseBody.Segments)
		}
	}
}

type showHistory struct {
	resp [][]string
	user string
	from string
	to   string
	want int
}

func TestShowUserHistoryHandler(t *testing.T) {

	//добавляем пользователей
	rnd := rand.New(rand.NewSource(time.Now().UnixNano()))
	t1 := "1" + fmt.Sprint(rnd.Int()%10000000)
	t2 := "2" + fmt.Sprint(rnd.Int()%10000000)
	t3 := "3" + fmt.Sprint(rnd.Int()%10000000)
	users := []addDeleteViewTestTable{
		{input: t1},
		{input: t2},
	}
	for _, user := range users {
		url := server + "/user/add?id=" + user.input
		result := makeRequest("POST", url, nil, []Header{{Name: "Content-Type", Val: "application/json"}})
		defer result.Body.Close()

		if http.StatusOK != result.StatusCode {
			t.Errorf("handler returned wrong status code: got %v want %v (req: %s)",
				result.StatusCode, http.StatusOK, user.input)
		}
	}

	// добавляем сегменты
	ts1 := "1" + fmt.Sprint(rnd.Int()%1000000000)
	ts2 := "2" + fmt.Sprint(rnd.Int()%1000000000)
	ts3 := "3" + fmt.Sprint(rnd.Int()%1000000000)
	segments := []addSegmentTestTable{
		{addSegmentTestInputTable{slug: ts1, percent: "0"}, 200},
		{addSegmentTestInputTable{slug: ts2, percent: "0"}, 200},
		{addSegmentTestInputTable{slug: ts3, percent: "0"}, 200},
	}
	for _, segment := range segments {
		url := server + "/segment/add?slug=" + segment.input.slug + "&percent=" + segment.input.percent
		result := makeRequest("POST", url, nil, []Header{{Name: "Content-Type", Val: "application/json"}})
		defer result.Body.Close()

		if segment.want != result.StatusCode {
			t.Errorf("handler returned wrong status code: got %v want %v (req: %s)",
				result.StatusCode, segment.want, segment.input)
		}
	}

	// добавляем пользователей к сегментам
	userseg := []addSegmentsToUserTest{
		{ // user: ts1 ts2
			user:   t1,
			header: Header{Name: "Content-Type", Val: "application/json"},
			input: handlers.AddUserSegmentRequestBody{
				Add: []handlers.SegmentWithTTL{
					{Slug: ts1},
					{Slug: ts2},
				},
				Delete: []string{},
			}, want: 200,
		},
		{ // user: ts1 ts2
			user:   t1,
			header: Header{Name: "Content-Type", Val: "application/json"},
			input: handlers.AddUserSegmentRequestBody{
				Add: []handlers.SegmentWithTTL{
					{Slug: ts3},
				},
				Delete: []string{ts1, ts2},
			}, want: 200,
		},
	}
	for _, entry := range userseg {
		url := server + "/user/" + entry.user + "/addsegments"
		result := makeRequest("POST", url, entry.input, []Header{entry.header})
		defer result.Body.Close()

		if entry.want != result.StatusCode {
			t.Errorf("handler returned wrong status code: got %v want %v (req: %s)",
				result.StatusCode, entry.want, entry.input)
		}
	}
	tests := []showHistory{
		{
			user: t1,
			resp: [][]string{
				{"User", "Segment", "Action", "Time (UTC+0)"},
				{t1, ts1, "Addition", ""},
				{t1, ts2, "Addition", ""},
				{t1, ts3, "Addition", ""},
				{t1, ts1, "Deletion", ""},
				{t1, ts2, "Deletion", ""},
			},
			want: 200,
			from: "2010-01-01",
			to:   time.Now().UTC().Add(time.Hour * 48).Format("2006-01-02")},
		{
			user: t2,
			resp: [][]string{
				{"User", "Segment", "Action", "Time (UTC+0)"},
			},
			want: 200,
			from: "2010-01-01",
			to:   time.Now().UTC().Add(time.Hour * 48).Format("2006-01-02")},
		{
			user: t3,
			resp: [][]string{
				{"User", "Segment", "Action", "Time (UTC+0)"},
			},
			want: 404,
			from: "2010-01-01",
			to:   time.Now().UTC().Add(time.Hour * 48).Format("2006-01-02")},
		{
			user: t2,
			resp: [][]string{
				{"User", "Segment", "Action", "Time (UTC+0)"},
			},
			want: 404,
			from: "2010/01/01",
			to:   time.Now().UTC().Add(time.Hour * 48).Format("2006-01-02")},
		{
			user: t2,
			resp: [][]string{
				{"User", "Segment", "Action", "Time (UTC+0)"},
			},
			want: 404,
			from: "2010-01-01",
			to:   "sdf"},
		{
			user: t2,
			resp: [][]string{
				{"User", "Segment", "Action", "Time (UTC+0)"},
			},
			want: 404,
			from: "asd",
			to:   "sdf"},
	}

	for _, test := range tests {
		url := server + "/user/" + test.user + "/history?from=" + test.from + "&to=" + test.to
		result := makeRequest("GET", url, nil, []Header{})
		defer result.Body.Close()

		if test.want != result.StatusCode {
			t.Errorf("handler returned wrong status code: got %v want %v (req: %s)",
				result.StatusCode, test.want, test.user)
		}
		if test.want == 200 {
			body, err := io.ReadAll(result.Body)
			if err != nil {
				t.Errorf("failed to parse responce: %s  (req: %s)", err, test.user)
			}
			resURL := string(body)
			if resURL[len(body)-4:len(body)] != ".csv" {
				t.Errorf("incorrect responce (doesn't end with .csv): %s  (req: %s)", string(body), test.user)
			}
			result = makeRequest("GET", resURL, nil, []Header{})
			defer result.Body.Close()
			fileReader := csv.NewReader(result.Body)
			records, err := fileReader.ReadAll()
			if err != nil {
				t.Errorf("failed to parse responce: %s  (req: %s)", err, test.user)
			}
			for i := range records {
				if i != 0 {
					records[i][3] = ""
				}
			}
			assert.Equal(t, test.resp, records)
		}
	}

}

func TestUpdateTTLHandler(t *testing.T) {

	result := makeRequest("POST", server+"/checkTTL", nil, []Header{{Name: "Content-Type", Val: "application/json"}})
	defer result.Body.Close()

	if http.StatusOK != result.StatusCode {
		t.Errorf("handler returned wrong status code: got %v want %v",
			result.StatusCode, http.StatusOK)
	}
	if result.StatusCode == 200 {
		body, err := io.ReadAll(result.Body)
		if err != nil {
			t.Errorf("failed to parse responce: %s", err)
		}
		var responseBody struct {
			Msg string `json:"msg"`
		}
		err = json.Unmarshal(body, &responseBody)
		if err != nil {
			t.Errorf("failed to parse responce: %s", err)
		}
		rightResponce := struct {
			Msg string
		}{"TTLs updated successfully!"}
		assert.Equal(t, rightResponce.Msg, responseBody.Msg)
	}
}

// создать запрос к ресурсу типа method
// к конечной точке url. Также передается тело запроса и проверка аутентификации
//
// Возвращает результат запроса
func makeRequest(method, url string, body interface{}, headers []Header) *http.Response {
	requestBody, err := json.Marshal(body) //тело запроса
	if err != nil {
		log.Print(err)
		requestBody = nil
	}
	request, err := http.NewRequest(method, url, bytes.NewBuffer(requestBody)) //создаем запрос
	if len(headers) != 0 {
		for _, h := range headers {
			request.Header.Add(h.Name, h.Val)
		}
	}
	if err != nil {
		log.Print(err)
		return nil
	}
	result, err := http.DefaultClient.Do(request) //выолняем запрос
	if err != nil {
		log.Printf("клиент: произошла ошибка выполнения запроса: %s\n", err)
		return nil
	}

	return result //результат отправляем обратно
}
